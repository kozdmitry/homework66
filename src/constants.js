export const CATEGORIES = [
    {title: 'Home', id: 'home'},
    {title: 'About', id: 'about'},
    {title: 'Contacts', id: 'contacts'},
    {title: 'Gallery', id: 'gallery'},
]