import axios from "axios";

const axiosPages = axios.create({
    baseURL: 'https://js9-hw65-default-rtdb.firebaseio.com/pages/'
});


export default axiosPages;