import React from "react";
import {Switch, Route} from "react-router-dom";
import StaticPages from "./container/StaticPages/StaticPages";
import Layout from "./components/Layout/Layout";
import EditPages from "./container/EditPages/EditPages";

const App =() => (
    <Layout>
        <Switch>
            <Route path="/pages/admin" component={EditPages} />
            <Route path="/pages/:name" exact component={StaticPages}/>
        </Switch>
    </Layout>

);

export default App;
