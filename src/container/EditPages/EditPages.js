import React, {useEffect, useState} from 'react';
import "./EditPages.css";
import {Button, Grid, MenuItem, Paper, TextField, Typography} from "@material-ui/core";
import axiosPages from "../../axiosPages";

const EditPages = ({history}) => {
    const [pages, setPages] = useState([]);
    const [pageChange, setPageChange] = useState('contacts');
    const [page, setPage] = useState({});

    useEffect(() => {
        axiosPages.get('.json').then(result => {
            const response = (result.data)
            const list =Object.keys(response).map(key => {
                return key
            })
            setPages(list)
        }, e => {
            console.log(e);
        });
    }, []);

    useEffect(() => {
        const getPage = async () => {
            try {
                const response = await axiosPages.get(pageChange + '.json')
                setPage(response.data);
            } catch (e) {
                console.log(e);
            }
        }
        getPage();
    }, [pageChange])

    const editPage = async (e) => {
        e.preventDefault();
        try {
            await axiosPages.put(pageChange + '.json', {...page})
            history.push('/pages/' + pageChange);
        } catch (e) {
            console.log(e);
        }

    };

    const changePage = e => {
        const {name, value} = e.target;
        setPage(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const selectPage = async (e) => {
        setPageChange(e.target.value);
    }

    return (
        <Paper className="container">
            <form onSubmit={editPage}>
                <Grid container direction="column" spacing={2}>
                    <Typography />
                    <Grid item xs={3} spacing={2}>
                        <Paper spacing={3}>
                            <Typography variant="h4">Edit pages</Typography>
                        </Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography pb={5}>Select page</Typography>
                    </Grid>
                    <Grid item xs={6} spacing={2}>
                        <TextField
                            onChange={(e) => selectPage(e)}
                            fullWidth
                            select
                            label="Title"
                            name="title"
                            required
                            spacing={3}
                            variant="outlined"
                            value={pageChange}
                        >
                            {pages.map(c => (
                                <MenuItem key={c} value={c}>{c}</MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={6} spacing={2}>
                        <TextField
                            placeholder="Title"
                            name="title"
                            variant="outlined"
                            onChange={changePage}
                            value={page.title}
                        />
                    </Grid>
                    <Grid item xs spacing={2}>
                        <TextField
                            fullWidth
                            multiline
                            placeholder="Content"
                            name="content"
                            rows={4}
                            variant="outlined"
                            onChange={changePage}
                            value={page.content}
                        />
                    </Grid>
                    <Grid item>
                        <Button type="submit" variant="outlined" color="primary">Save</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default EditPages;