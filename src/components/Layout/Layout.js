import React from 'react';
import "./Layout.css";
import {
    AppBar,
    Button,
    CssBaseline, Drawer, Grid,
    IconButton,
    Link,
    MenuItem,
    MenuList,
    Toolbar,
    Typography
} from "@material-ui/core";
import {CATEGORIES} from "../../constants";
import {NavLink} from "react-router-dom";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Typography variant="h6" noWrap>
                                Static Pages
                            </Typography>
                        </Grid>
                        <Grid item>
                            {/*<Button component={NavLink} to="/" color="inherit">Home</Button>*/}
                            {CATEGORIES.map(c => (
                                <Button key={c.id} component={NavLink} to={"/pages/" + c.id} color="inherit">{c.title}</Button>
                            ))}
                            <Button component={NavLink} to="/pages/admin" color="inherit">Admin</Button>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <main>
                {children}
            </main>

        </>
    );
};

export default Layout;