import React, {useEffect, useState} from 'react';
import Modal from "../../components/UI/Modal/Modal";
import Spinner from "../../components/UI/Spinner/Spinner";
import axiosPages from "../../axiosPages";
import ErrorComponents from "../../components/UI/ErrorComponent/ErrorComponents";

const withLoader = (WrappedComponent, axios) => {
    return function WithLoader (props) {
        const [loading, setLoading] = useState(false);
        const [pageError, setPageError] = useState(null);

        useEffect(() => {
            axiosPages.interceptors.request.use(req => {
                console.log('[In request interceptor]', req);
                setLoading(true);
                return req;
            });
        },[])


        useEffect(() => {
            axios.interceptors.response.use(response => {
                console.log('[in hoc interceptor]', response);
                setLoading(false);
                return response;
            }, error => {
                setLoading(false);
                setPageError(error);
                throw error;
            })
        }, []);

        const errorDismissed = () => {
            setLoading(false);
        }

        return (
            <>
            <Modal show={!!loading} clossed={errorDismissed}>
                {loading && <Spinner/>}
            </Modal>
                {pageError ? <ErrorComponents/> : <WrappedComponent {...props}/>}
            </>
        );
    };
};

export default withLoader;